package ru.t1.nikitushkina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.nikitushkina.tm.api.endpoint.IProjectEndpoint;
import ru.t1.nikitushkina.tm.command.AbstractCommand;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.dto.model.ProjectDTO;

import java.util.List;

@Component
public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderProjects(@Nullable final List<ProjectDTO> projects) {
        int index = 1;
        for (@Nullable final ProjectDTO project : projects) {
            showProject(project);
            index++;
        }
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("USER ID: " + project.getUserId());
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS:" + Status.toName(project.getStatus()));
    }

}
